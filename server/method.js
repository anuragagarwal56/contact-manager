Meteor.methods({
  addContact: function(contact){
    check(contact, {
      first_name: String,
      last_name: String,
      address: String,
      phone: String,
      email: String,
      image: Match.Optional(String),
      created_at: Date,
      owner: String
    })
    if(!Meteor.userId() ||  contact.owner !== Meteor.userId())
      throw new Meteor.Error("not-authorized");
    return Contacts.insert(contact);
  },
  removeContact: function(contactId){
    check(contactId, String);
    if(!Meteor.userId() ||  Contacts.findOne(contactId).owner !== Meteor.userId())
      throw new Meteor.Error("not-authorized");
    Contacts.remove(contactId);
  },
  updateContact: function(contactId, contact){
    check(contact, {
      first_name: String,
      last_name: String,
      address: String,
      phone: String,
      email: String,
      image: Match.Optional(String),
      created_at: Match.Optional(Date),
      owner: Match.Optional(String)
    });
    check(contactId, String);
    if(!Meteor.userId() || Contacts.findOne(contactId).owner !== Meteor.userId())
      throw new Meteor.Error("not-authorized");
    Contacts.update(contactId, {$set: contact});
    if(!contact["image"])
      Contacts.update(contactId, {$unset: {"image": ""}});
  }
})
